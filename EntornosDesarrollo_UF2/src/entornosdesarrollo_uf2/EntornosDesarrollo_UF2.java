package entornosdesarrollo_uf2;

import java.util.Scanner;

/**
 * @author mateodecu@gmail.com
 */
public class EntornosDesarrollo_UF2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int base;
        int altura;
        
        System.out.println("Bienvenido\nPor favor ingrese la base y altura del triangulo a conocer");
        
        System.out.println("Base (cm):");
        base = scanner.nextInt();
        
        System.out.println("Altura (cm):");
        altura = scanner.nextInt();
        
        Triangulo triangulo = new Triangulo(base, altura);

        System.out.println("El area del triangulo ingresado es de " + triangulo.getArea() + " cm2"); 
    }    
}
